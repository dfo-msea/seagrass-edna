# Seagrass eDNA

__Main author: Emily Rubdige    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: emily.rubidge@dfo-mpo.gc.ca | tel: 


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
  + [Subsections within contents](#subsections-within-contents)
- [Methods](#methods)
  + [Subsections within methods](#subsections-within-methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Biodiversity monitoring in seagrass habitat using eDNA.


## Summary


## Status
In development.

## Contents
Describe the contents of the repository. Are there multiple scripts or directories? What is their purpose and how do they relate to each other?
### Subsections within contents
Use subsections to describe the purpose of each script or sub-directories if warranted.


## Methods
Describe the procedure followed in as much detail as possible (e.g., the steps taken). 
### Subsections within methods
If helpful, organise the methods under multiple subheadings. These subheading could be scripts or sub-directories (if they exist) or analytical tasks (e.g., querying data, spatial analyses, data processing, QAQC checks).


## Requirements
*Optional section.* List the input data requirements or software requirements to successfully execute the code.


## Caveats
Anything other users should be aware of including gaps in the input or output data and warnings about appropriate use.


## Uncertainty 
*Optional section.* Is there some uncertainty associated with the output? Assumptions that were made?


## Acknowledgements 
*Optional section.* List any contributors, people or institutions.


## References 
*Optional section.* 
